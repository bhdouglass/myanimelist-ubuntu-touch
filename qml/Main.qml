import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0
import QtWebEngine 1.7
import Lomiri.Components 1.3 as Lomiri
import Morph.Web 0.1

import "Components" as Components

ApplicationWindow {
    id: root

    visible: true
    width: units.gu(50)
    height: units.gu(75)

    Settings {
        id: settings
        property string lastUrl: 'https://myanimelist.net'
        property string username
        property int defaultAnimeList: 0
        property int defaultMangaList: 0
    }

    WebContext {
        id: webcontext
        userAgent: 'Mozilla/5.0 (Linux; Android 8.0; Nexus 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.151 Mobile Safari/537.36 Ubuntu Touch Webapp'
        offTheRecord: false
    }

    WebView {
        id: webview
        anchors {
            top: parent.top
            bottom: nav.top
        }
        width: parent.width
        height: parent.height

        context: webcontext
        url: settings.lastUrl
        onUrlChanged: {
            var strUrl = url.toString();
            if (settings.lastUrl != strUrl && strUrl.match('(http|https)://myanimelist.net/(.*)')) {
                settings.lastUrl = strUrl;
            }
        }

        function navigationRequestedDelegate(request) {
            var url = request.url.toString();
            var isvalid = false;

            if (!url.match('(http|https)://myanimelist.net/(.*)') && request.isMainFrame) {
                Qt.openUrlExternally(url);
                request.action = WebEngineNavigationRequest.IgnoreRequest;
            }
        }
    }

    ProgressBar {
        height: units.dp(3)
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }

        value: (webview.loadProgress / 100)
        visible: (webview.loading && !webview.lastLoadStopped)
    }

    // TODO conver this to be qqc2/suru style
    Components.BottomNavigationBar {
        id: nav

        function animeList() {
            if (settings.username) {
                var url = 'https://myanimelist.net/animelist/' + settings.username;
                if (settings.defaultAnimeList == 0) {
                    url += '?status=7';
                }
                else if (settings.defaultAnimeList == 1) {
                    url += '?status=1';
                }
                else if (settings.defaultAnimeList == 2) {
                    url += '?status=2';
                }
                else if (settings.defaultAnimeList == 3) {
                    url += '?status=3';
                }
                else if (settings.defaultAnimeList == 4) {
                    url += '?status=4';
                }
                else if (settings.defaultAnimeList == 5) {
                    url += '?status=6';
                }

                return url;
            }

            return null;
        }

        function mangaList() {
            if (settings.username) {
                var url = 'https://myanimelist.net/mangalist/' + settings.username;
                if (settings.defaultMangaList == 0) {
                    url += '?status=7';
                }
                else if (settings.defaultMangaList == 1) {
                    url += '?status=1';
                }
                else if (settings.defaultMangaList == 2) {
                    url += '?status=2';
                }
                else if (settings.defaultMangaList == 3) {
                    url += '?status=3';
                }
                else if (settings.defaultMangaList == 4) {
                    url += '?status=4';
                }
                else if (settings.defaultMangaList == 5) {
                    url += '?status=6';
                }

                return url;
            }

            return null;
        }

        selectedIndex: -1 // Don't show any items as active
        model: [
            {
                'name': i18n.tr('Home'),
                'iconName': 'home',
                'url': 'https://myanimelist.net',
            },
            {
                'name': i18n.tr('Anime List'),
                'iconName': 'view-list-symbolic',
                'url': animeList(),
            },
            {
                'name': i18n.tr('Manga List'),
                'iconName': 'view-list-symbolic',
                'url': mangaList(),
            },
            {
                'name': i18n.tr('Settings'),
                'iconName': 'settings',
                'url': null,
            }
        ]

        onTabThumbClicked: {
            if (model[index].url) {
                webview.url = model[index].url;
            }
            else {
                settingsDialog.open()
            }
        }
    }

    Dialog {
        id: settingsDialog

        width: parent.width - units.gu(10)

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        parent: ApplicationWindow.overlay

        modal: true
        title: i18n.tr('Settings')

        function save() {
            settings.username = user.text;
            settings.defaultAnimeList = defaultAnime.currentIndex;
            settings.defaultMangaList = defaultManga.currentIndex;
            settingsDialog.close();
        }

        ColumnLayout {
            spacing: units.gu(1)
            anchors.fill: parent

            Label {
                text: i18n.tr('MAL username')
            }

            TextField {
                id: user
                Layout.fillWidth: true

                text: settings.username

                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                onAccepted: settingsDialog.save()
            }

            Label {
                text: i18n.tr('Default Anime List')
            }

            ComboBox {
                id: defaultAnime
                Layout.fillWidth: true

                model: [
                    i18n.tr('All'),
                    i18n.tr('Watching'),
                    i18n.tr('Completed'),
                    i18n.tr('On Hold'),
                    i18n.tr('Dropped'),
                    i18n.tr('Plan to Watch'),
                ]
            }

            Label {
                text: i18n.tr('Default Manga List')
            }

            ComboBox {
                id: defaultManga
                Layout.fillWidth: true

                model: [
                    i18n.tr('All'),
                    i18n.tr('Reading'),
                    i18n.tr('Completed'),
                    i18n.tr('On Hold'),
                    i18n.tr('Dropped'),
                    i18n.tr('Plan to Read'),
                ]
            }

            Button {
                Layout.fillWidth: true
                Layout.topMargin: units.gu(1)
                Layout.bottomMargin: units.gu(1)
                text: i18n.tr('OK')

                onClicked: settingsDialog.save()
            }
        }
    }

    Connections {
        target: Lomiri.UriHandler
        onOpened: {
            webview.url = uris[0];
        }
    }

    Component.onCompleted: {
        if (Qt.application.arguments[1] && Qt.application.arguments[1].indexOf('myanimelist.net') >= 0) {
            webview.url = Qt.application.arguments[1];
        }
    }
}
